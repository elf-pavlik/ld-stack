# Solid + LDF

## Use case

Very often given dataset has:
* **write permissions restricted to list of agents**
* **public read permissions**

for example collaboration assisting tools like Gitlab, Github, Bitbucket etc.

[Solid](), combines [Linked Data Platform]() with [Web Access Control]() providing decentralized way to write to restricted datasets. [Linked Data Fragments]() provide a powerful ways to query data, for example [Triple Pattern Fragments]() which can work very well with libraries like [Clownface]().

## POC

Let's follow the Gitlab, Github, Bitbucket use case. We find there *People* who can have memberships in multiple *Organizations*. We also have *Projects* which either a person or organization can own (agents can transfer ownership of a project!). Organizations can also have *Teams*, for example project specific teams, coordination teams etc. Projects have *Tasks*, in our case creating new tasks also has restricted access, but everyone can send [Linked Data Notification]() based on which project team can create one or more tasks.

To ground our proof of concept in real world, let's focus on:

### Groups

* RDF/JS CG - W3C
* RDF/JS Representation TF
* Hydra CG - W3C
* Linked Data Fragments
* Digital Bazaar
* OpenLink Software

### Projects

#### specs
* Solid
* Web Access Control
* Linked Data Fragments
* Triple Pattern Fragment
* RDF/JS Core
* [RDF Dataset Normalization](https://json-ld.github.io/normalization/spec/)
* [Linked Data Signatures 1.0](https://web-payments.org/specs/source/ld-signatures/)

#### code
* node-solid-server
* solid-client
* rdflib.js
* rdf-ext
* SimpleRDF
* Clownface
* LDF Server node.js
* LDF Client
* N3.js
* jsonld.js
* bedrock
* rdf-editor

### People

* elf Pavlik - https://elf-pavlik.hackers4peace.net/
* Thomas Bergwinkl - https://www.bergnet.org/people/bergi/card#me
* Dmitri Zagidulin -
* Ruben Verborgh -
* Nicola Greco -
* Andrei Sambra - https://deiu.me/profile#me
* Melvin Carvalho - https://melvincarvalho.com/#me
* Tim Berners-Lee - https://www.w3.org/People/Berners-Lee/card#i
* Manu Sporny
